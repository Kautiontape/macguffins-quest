$(document).keyup(function(event) {
  if(event.key == "Enter") {
    console.log(event);
    if ($("#addQuest").is(":focus") || $("#questSize").is(":focus")) {
      $("#addQuestBtn").click();
    }
    
    if ($("#addPlayer").is(":focus")) {
      $("#addPlayerBtn").click();
    }
  }
});